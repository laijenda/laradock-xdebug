echo 'Stop xDebug'

    # Comment out xdebug extension line
    OFF_CMD="sed -i 's/^zend_extension=/;zend_extension=/g' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini"

    # If running on Windows, need to prepend with winpty :(
    if [[ $OS_TYPE == "MINGW" ]]; then
        # This is the equivalent of:
        # winpty docker exec -it laradock_php-fpm_1 bash -c 'bla bla bla'
        # Thanks to @michaelarnauts at https://github.com/docker/compose/issues/593
        winpty docker exec -it laradock_php-fpm_1 bash -c "${OFF_CMD}"
        winpty docker restart laradock_php-fpm_1
        #docker-compose restart php-fpm
        winpty docker exec -it laradock_php-fpm_1 bash -c 'php -v'

    else
        winpty docker exec -it laradock_php-fpm_1 bash -c "${OFF_CMD}"
        # docker-compose restart php-fpm
        winpty docker restart laradock_php-fpm_1
        winpty docker exec -it laradock_php-fpm_1 bash -c 'php -v'
    fi
