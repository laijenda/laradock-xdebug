echo 'Start xDebug'

    # And uncomment line with xdebug extension, thus enabling it
    ON_CMD="sed -i 's/^;zend_extension=/zend_extension=/g' \
                    /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini"

    # If running on Windows, need to prepend with winpty :(
    if [[ $OS_TYPE == "MINGW" ]]; then
        winpty docker exec -it laradock_php-fpm_1 bash -c "${ON_CMD}"
        docker restart laradock_php-fpm_1
        winpty docker exec -it laradock_php-fpm_1 bash -c 'php -v'

    else
      echo 'ok'
        winpty docker exec -it laradock_php-fpm_1 bash -c "${ON_CMD}"
        winpty docker restart laradock_php-fpm_1
        winpty docker exec -it laradock_php-fpm_1 bash -c 'php -v'
    fi
